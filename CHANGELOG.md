# TWOverflow

## twoverflow v2.0 changes

### FarmOverflow
- many string identifications changed to snek case.
- language keys changed to snek case and remove the contexts.
- maxTravelTime settings changed from time str to minutes int.
- groupIgnore variable now is group id and not the group object.
- groupInclude can now have multiple groups. Array with group ids.
- groupOnly same as groupInclude
- presetName setting can now have multiple presets. Array with preset objects.
- presetName setting renamed to presets
